module Compilers where

import qualified Data.Set               as S
import           Hakyll
import           Text.Pandoc.Definition
import           Text.Pandoc.Options

myCompiler :: Compiler (Item String)
myCompiler = pandocMathCompiler myTransform

myTransform :: Pandoc -> Pandoc
myTransform p@(Pandoc meta blocks) = (Pandoc meta (block:blocks))
  where
    block = Para
        [ Link
            nullAttr
            [ SmallCaps [Str "[ERT: ", Str $ timeEstimateString p ++ "]"] ]
            ("/posts/2016-06-05-estimated-reading-time-in-hakyll.html", "")
        ]

    timeEstimateString :: Pandoc -> String
    timeEstimateString = toClockString . timeEstimateSeconds

    toClockString :: Int -> String
    toClockString i
        | i >= 60 * 60 = show hours   ++ "h" ++ show minutes ++ "m" ++ show seconds ++ "s"
        | i >= 60      = show minutes ++ "m" ++ show seconds ++ "s"
        | otherwise    = show seconds ++ "s"
      where
        hours   = i `quot` (60 * 60)
        minutes = (i `rem` (60 * 60)) `quot` 60
        seconds = i `rem` 60


    timeEstimateSeconds :: Pandoc -> Int -- average adult reading speed is 300 WPM, that's 5 WPS
    timeEstimateSeconds = (`quot` 5) . nrWords

    readingSpeedWPS :: Int
    readingSpeedWPS = 5

    nrWords :: Pandoc -> Int
    nrWords = (`quot` 5) . nrLetters

    nrLetters :: Pandoc -> Int
    nrLetters (Pandoc _ bs) = sum $ map cb bs
      where
        cbs = sum . map cb
        cbss = sum . map cbs
        cbsss = sum . map cbss

        cb :: Block -> Int
        cb (Plain is) = cis is
        cb (Para is) = cis is
        cb (CodeBlock _ s) = length s
        cb (RawBlock _ s) = length s
        cb (BlockQuote bs) = cbs bs
        cb (OrderedList _ bss) = cbss bss
        cb (BulletList bss) = cbss bss
        cb (DefinitionList ls) = sum $ map (\(is, bss) -> cis is + cbss bss) ls
        cb (Header _ _ is) = cis is
        cb HorizontalRule = 0
        cb (Table is _ _ tc tcs) = cis is + cbss tc + cbsss tcs
        cb (Div _ bs) = cbs bs
        cb Null = 0

        cis = sum . map ci
        ciss = sum . map cis

        ci :: Inline -> Int
        ci (Str s) = length s
        ci (Emph is) = cis is
        ci (Strong is) = cis is
        ci (Strikeout is) = cis is
        ci (Superscript is) = cis is
        ci (Subscript is) = cis is
        ci (SmallCaps is) = cis is
        ci (Quoted _ is) = cis is
        ci (Cite _ is) = cis is
        ci (Code _ s) = length s
        ci Space = 1
        ci SoftBreak = 1
        ci LineBreak = 1
        ci (Math _ s) = length s
        ci (RawInline _ s) = length s
        ci (Link _ is (_, s)) = cis is + length s
        ci (Image _ is (_, s)) = cis is + length s
        ci (Note bs) = cbs bs

pandocMathCompiler :: (Pandoc -> Pandoc) -> Compiler (Item String)
pandocMathCompiler = pandocCompilerWithTransform readers writers
  where
    readers = defaultHakyllReaderOptions {
                    readerExtensions = pandocExtensions
                  , readerSmart = True
                  }
    writers = defaultHakyllWriterOptions {
                    writerHTMLMathMethod = MathJax ""
                  , writerExtensions = foldr S.insert (writerExtensions defaultHakyllWriterOptions) $ mathExtensions ++ codeExtensions
                  }

    codeExtensions =    [
                            Ext_backtick_code_blocks
                        ,   Ext_inline_code_attributes
                        ,   Ext_literate_haskell
                        ]

    mathExtensions =    [
                            Ext_tex_math_dollars
                        ,   Ext_tex_math_double_backslash
                        ,   Ext_latex_macros
                        ]

