{-# LANGUAGE OverloadedStrings #-}

module TechnicalCfg where

import           Data.List (intersperse)

import           Constants

--[ Technical config ]----------------------------------------------------------

deployCmd :: String
deployCmd = concat $ intersperse " && " [comp, clean, rebuild, tidy, upload]

comp :: String
comp = unwords $ program:args
    where
        program = "make"
        args = ["--jobs"]

clean :: String
clean = unwords $ program:args
    where
        program = "site"
        args = ["clean"]


rebuild :: String
rebuild = unwords $ program:args
    where
        program = "site"
        args = ["rebuild"]

tidy :: String
tidy = "./script/tidy_html.sh"

upload :: String
upload = unwords $ program:flags ++ [source, destination]
    where
        program = "rsync"
        flags   = [
                        "--archive"
                    ,   "--verbose"
                    ,   "--recursive"
                    ,   "--ipv6"
                    ,   "--human-readable"
                    ,   "--checksum"
                  ]
        source = outputDir ++ "/*"
        destination = "blog:/www/cs-syd.eu"

