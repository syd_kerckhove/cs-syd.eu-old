{-# LANGUAGE OverloadedStrings #-}

module Patterns where

import           Hakyll

import           Constants

templatesPattern  :: Pattern
templatesPattern  = fromGlob $ templatesDirectoryName ++ "/*"

assetsPattern     :: Pattern
assetsPattern     = fromGlob $ assetsDirectoryName ++ "/**"

stylePattern      :: Pattern
stylePattern      = fromGlob $ styleDirectoryName ++ "/**"

quotesPattern     :: Pattern
quotesPattern     = fromGlob $ quotesDirectoryName ++ "/**"

postsPattern      :: Pattern
postsPattern      = fromGlob $ postsDirectoryName ++ "/**"

draftsPattern     :: Pattern
draftsPattern     = fromGlob $ draftsDirectoryName ++ "/**.md"

pagesPattern :: Pattern
pagesPattern = fromList
    [
      "index.html"
    , "archive.html"
    , "categories.html"
    , "quotes.html"
    , "tags.html"
    , "about.html"
    , "404.md"
    , "contact.html"
    , "todo.md"
    ]

sydManifestoPattern :: Pattern
sydManifestoPattern = "syd_manifesto/*.md"

projectsPattern :: Pattern
projectsPattern = "projects/*.md"

projectSuggestionsPattern :: Pattern
projectSuggestionsPattern = "projects/suggestions/*.md"

missingPagePattern :: Pattern
missingPagePattern = fromGlob $ missingPage

finishedPattern :: Pattern
finishedPattern = contentPattern

contentPattern :: Pattern
contentPattern = quotesPattern .||. postsPattern .||. sydManifestoPattern .||. projectSuggestionsPattern

taggedPattern :: Pattern
taggedPattern = quotesPattern .||. postsPattern .||. sydManifestoPattern .||. projectSuggestionsPattern

categorisedPattern :: Pattern
categorisedPattern = postsPattern

teaseredPattern :: Pattern
teaseredPattern = taggedPattern


