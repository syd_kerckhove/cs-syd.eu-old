---
layout: page
title: Xmonad
published: 2015-07-19
tags: xmonad
---

Xmonad is my primary window manager.
As you might have guessed, I enjoyed configuring xmonad very much.

The problem with an efficient xmonad configuration is that it's highly keyboard-dependent.

<div></div><!--more-->

I don't use any status bar, so all the magic happens in `~/.xmonad/`.

I'm not going to go over everything in the entire configuation, you can find [the files in my SUS depot](https://github.com/NorfairKing/sus-depot/blob/master/shared/xmonad).
I will just go into the interesting parts, specifically the parts that depend heavily on the keyboard.

## Modularity

Thanks to the [Super User Spark](https://github.com/NorfairKing/super-user-spark), I was allowed to both modularise my Xmonad configuration and remove duplication.
Without it I would either have to duplicate most of my configuration or have a monolithic configuration.

If you look closely at my `~/.xmonad` directory, you will see how far I tried to take the modularity:

```
- ~/.xmonad
 |- xmonad.hs
 |- lib
   |- Actions.hs
   |- Constants.hs
   |- Keys.hs
   |- Layout.hs
   |- Modifier.hs
   |- Solarized.hs
   |- Workspaces.hs
```

I deliberately tried to make my `xmonad.hs` file very small with plenty of imports.
The `Actions.hs` contains a lot of `X ()` functions.
The `Keys.hs` file contains maps of keys to `X ()` actions from the `Actions.hs`.
This way, only the `Keys.hs` file is the only file that has to be different on different systems.

## Workspaces
### [On a regular keyboard](https://github.com/NorfairKing/sus-depot/blob/master/shared/xmonad/xmonad.hs)
#### Definitions
On a regular keyboard, I'm going to make use of the numpad to define my workspaces.
The numbers from 1 to 9 each represent a workspace.
``` Haskell
-- The workspaces
bl = "1: Workflow"       -- Bottom   Left
bm = "2: Etc"            -- Bottom   Middle
br = "3: Mail"           -- Bottom   Right
ml = "4: Terminal"       -- Middle   Left
mm = "5: Development"    -- Middle   Middle
mr = "6: Internet"       -- Middle   Right
tl = "7: Chat"           -- Top      Left
tm = "8: Etc"            -- Top      Middle
tr = "9: Clip"           -- Top      Right
```

Xmonad requires them in a list, so we put then in a list:

``` Haskell
-- The workspaces list
myWorkspaces :: [WorkspaceId]
myWorkspaces =
  [
    tl, tm, tr,
    ml, mm, mr,
    bl, bm, br
  ]

-- | The workspace that will be on screen after launch
startupWorkspace = mr
```

#### Navigation
To navigate these workspaces, we can either jump directly to the workspace with `mod+n` or we can jump to adjacent workspaces with `mod+arrow`.
We can also move windows to other workspaces by adding `shift` to the modifier keys.
``` Haskell
-- | Shortcuts for navigating workspaces
workspaceNavigation :: [((KeyMask, KeySym), X ())]
workspaceNavigation =
    -- Navigate directly
    [
        (
            (m .|. myModMask, k)
            ,
            windows $ f i
        )
        |
        (i, k) <- zip myWorkspaces numPadKeys ++ zip myWorkspaces numKeys,
        (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]
    ]
        ++
    -- Navigate with arrow keys
    M.toList (planeKeys myModMask (Lines 4) Finite)


numPadKeys :: [KeySym]
numPadKeys =
  [
    xK_KP_Home, xK_KP_Up,    xK_KP_Page_Up,
    xK_KP_Left, xK_KP_Begin, xK_KP_Right,
    xK_KP_End,  xK_KP_Down,  xK_KP_Page_Down
  ]

numKeys :: [KeySym]
numKeys =
  [
    xK_7, xK_8, xK_9,
    xK_4, xK_5, xK_6,
    xK_1, xK_2, xK_3
  ]
```

### [On the Kinesis Advantage with my Dvorak layout](https://github.com/NorfairKing/sus-depot/blob/master/quintus/xmonad.hs)
#### Definitions
On the Kinesis Advantage there is no numpad, so I had to find another way of defining my workspaces.
I also wanted more than 9 workspaces.
The only other concept I could think of that met both requirements was *the entire alphabet*.

As you can see, two letters are commented out.
I use `mod-f` for opening a new terminal and `mod-y` to close the current window

``` Haskell
{- WORKSPACES -}

alphabet :: [Char]
alphabet = 
    [
                             'p', {-'y',-} {-'f',-} 'g',   'c',   'r',   'l',
        'a',   'o',   'e',   'u',   'i',     'd',   'h',   't',   'n',   's',
               'q',   'j',   'k',   'x',     'b',   'm',   'w',   'v',   'z'
    ]

workspacePrefix :: String
workspacePrefix = "w_"

myWorkspaces :: [WorkspaceId]
myWorkspaces = map (\x -> workspacePrefix ++ [x]) alphabet

workspaceKeys :: [KeySym]
workspaceKeys =
    [
                                xK_p, {-xK_y,-} {-xK_f,-} xK_g,   xK_c,   xK_r,   xK_l,
        xK_a,   xK_o,   xK_e,   xK_u,   xK_i,     xK_d,   xK_h,   xK_t,   xK_n,   xK_s,
                xK_q,   xK_j,   xK_k,   xK_x,     xK_b,   xK_m,   xK_w,   xK_v,   xK_z
    ]

workspaceMapping :: [(WorkspaceId, KeySym)]
workspaceMapping = zip myWorkspaces workspaceKeys

-- The workspace that will bee on screen after launch
startupWorkspace = workspacePrefix ++ ['a']
```

#### Navigation
Navigation happens in about the same way.
There is no navigating by arrow keys on the Kinesis Advantage though.
``` Haskell
-- | Shortcuts for navigating workspaces
workspaceNavigation :: [((KeyMask, KeySym), X ())]
workspaceNavigation =
    -- Navigate directly
    [
        (
            (m .|. myModMask, k)
            ,
            windows $ f i
        )
        |
        (i, k) <- zip myWorkspaces numPadKeys ++ zip myWorkspaces numKeys,
        (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]
    ]
```

#### Layouts
There is generally nothing special about layouts, but using the golden ratio seemed interesting:

``` Haskell
myLayoutHook = avoidStruts (full ||| tiled ||| mtiled )
    where
        -- Fullscreen (default)
        full    = named "full" $ spacing tileSpacing $ noBorders Full
        -- Split vertically with phi as the ratio between the widths
        tiled   = named "tiled" $ spacing tileSpacing $ Tall 1 (5/100) (1/phi)
        -- Split horizonatlly in the same way
        mtiled  = named "mtiled" $ Mirror tiled
        -- The golden ratio
        phi = toRational $ (1 + sqrt 5) / 2

```



