{-# LANGUAGE OverloadedStrings #-}

module Style where

import           Hakyll

import           Constants
import           Patterns

-- Css
lessCompiler :: String
lessCompiler = "lessc"

styleRules :: Rules ()
styleRules = do
    match stylePattern $ do
        route   idRoute
        compile getResourceBody

    create [styleSheetIdentifier] $ do
        route idRoute
        compile $ do
            getMatches stylePattern
            loadBody styleSheetSourceIdentifier
                >>= unixFilter lessCompiler
                                    [
                                        "-"
                                    ,   "--clean-css"
                                    ,   "--include-path=" ++ styleDirectoryName
                                    -- ,   "-O2"
                                    ]
                >>= makeItem

