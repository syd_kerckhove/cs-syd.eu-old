set -e # Abort on error

echo "Checking whether you didn't mess anything up."

make
site rebuild

./script/tidy_html.sh

site check --internal-links

echo "Okay, you're fine."
