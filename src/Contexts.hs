module Contexts where

import           Constants
import           GeneralCfg
import           Hakyll

rootCtx :: Context String
rootCtx = mconcat
    [
        -- Non-technical
        constField "superTitle" blogTitle

    ,   constField "language" language

    ,   constField "Author" author
    ,   constField "github" github
    ,   constField "twitter" twitter

        -- Technical
    ,   constField "initial_scale" $ show initial_scale
    ,   constField "assets" assetsDirectory
    ,   constField "style" styleSheet
    ,   constField "logo" logoFile
    ,   constField "blogroot" blogRoot

    ,   defaultContext
    ]

postCtx :: Context String
postCtx = mconcat
    [
        dateField "date" "%B %d %Y"
    ,   dateField "strictDate" "%Y-%m-%d"
    ,   htmlfreeTeaserField "teaser" "content"
    ,   rootCtx
    ]

separator :: String
separator = "<!--more-->"

htmlfreeTeaserField :: String           -- ^ Key to use
                    -> Snapshot         -- ^ Snapshot to load
                    -> Context String   -- ^ Resulting context
htmlfreeTeaserField key snapshot = field key $ \item -> do
    body <- itemBody <$> loadSnapshot (itemIdentifier item) snapshot
    case needlePrefix separator body of
        Nothing -> fail $
            "Hakyll.Web.Template.Context: no teaser defined for " ++
            show (itemIdentifier item)
        Just t -> return $ trim $ stripTags t

sydManifestoPageCtx :: Context String
sydManifestoPageCtx = postCtx

projectsPageCtx :: Context String
projectsPageCtx = postCtx
