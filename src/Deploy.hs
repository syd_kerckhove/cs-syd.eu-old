module Deploy where

import           System.Process

postReady :: IO ()
postReady = do
    system $ "git tag -a $(date --date sunday +%Y-%m-%d)"
    system $ "git push --tags"
    return ()

checkoutToday :: IO ()
checkoutToday = do
    system $ "git pull"
    system $ "git checkout $(date +%Y-%m-%d)"
    return ()
