---
layout: page
title: Hacyc
description: Haskell Curses Youtube Client
published: 2015-08-16
last-updated: 2015-08-16
tags: hacyc, youtube, CLI
---

While slowly replacing every part of my user experience of a computer to go through a [command line interface](/posts/2014-07-29-why-i-dont-like-guis.html) I noticed that I still don't have a good alternative to watching Youtube videos.

- Curses youtube browser
- Haskell youtube API
- A lot of _configurable_ shortcuts
- Configurations for different kinds of internet speeds
- Conform to Xdg
- Use the goole discovery API?
- Mark items for caching before watching them
- in general: a better youtube experience for power users
