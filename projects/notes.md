---
layout: page
title: Notes
published: 2015-07-04
last-updated: 2015-07-04
tags: notes, math
---

I have a weird way of studying mathematical subjects.
I make notes, lots of notes.
Those notes are available for anyone to use, correct or help out with.

#### English

- [The Notes](https://github.com/NorfairKing/the-notes):<br>
  All my english notes in one repository.
  They're written in Haskell using HaTeX to allow for subset-publishing.
  Any subset of the notes can be published seperately as long as it's internally coherent.
- [All notes](/books/the-notes/out/the-notes.pdf)
- [Order Theory](/books/the-notes/out/order-theory.pdf)
- [Cryptography](/books/the-notes/out/cryptography.pdf)

##### ETH Algolab Guide
- [ETH Algolab](https://github.com/NorfairKing/eth-algolab-2015) [PDF](/books/guide.pdf)

##### Card game
- [CN Cards](https://github.com/NorfairKing/cn-cards) [PDF](http://cs-syd.eu/raw/cn-cards.pdf):<br> A card game about computer networks


#### Dutch

I used to write my notes in Dutch.
These are notes for specific courses at KU Leuven in Belgium.
For more general notes, refer to `the-notes` above.

- [Analyse I](https://github.com/NorfairKing/analyse-notities): [PDF](/books/analyse-notities.pdf)
- [Algebra I](https://github.com/NorfairKing/algebra-notities): [PDF](/books/algebra-notities.pdf)
- [Automaten en Berekenbaarheid](https://github.com/NorfairKing/ab-notities): [PDF](/books/ab-notities.pdf)
- [Gegevensbanken](https://github.com/NorfairKing/Gegevensbanken-Tutorials): [PDF](/books/gegevensbanken-tutorials.pdf)
- [Kansrekenen](https://github.com/NorfairKing/kansrekenen-notities): [PDF](/books/kansrekenen-notities.pdf)
- [Lineaire Algebra](https://github.com/NorfairKing/lineairealgebra)
- [Meetkunde I](https://github.com/NorfairKing/meetkunde-notities): [PDF](/books/meetkunde-notities.pdf)
- [Numerieke Wiskunde](https://github.com/NorfairKing/all-you-can-carry): [PDF](/books/all-you-can-carry.pdf)
- [Toepassingen van Meetkunde](https://github.com/NorfairKing/TMI-Notities): [PDF](/books/TMI-notities.pdf)
- [Wiskunde II](https://github.com/NorfairKing/Wiskunde-II-Oplossingen-van-Oefeningen): [PDF](http://cs-syd.eu/raw/oplossingen.pdf)

