# cs-syd
My personal blog

## Clone recursively
`git submodule update --init --recursive`


## Hooks
To keep consistency at all times, deploy the git hook using `spark deploy hooks.sus`


## Dependencies
- `lessc`, but only a version older than `2.0` works...
- `less` plugins: `clean-css`
