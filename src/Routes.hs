module Routes where

import           Hakyll

import           Constants

routeOut :: Rules ()
routeOut = route $ setExtension outputExtension

routeId :: Rules ()
routeId = route idRoute
