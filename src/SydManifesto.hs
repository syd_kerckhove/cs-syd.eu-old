{-# LANGUAGE OverloadedStrings #-}

module SydManifesto where

import           Hakyll

import           Compilers
import           Constants
import           Contexts
import           Patterns

sydManifestoRules :: Rules ()
sydManifestoRules = do
    match sydManifestoPattern $ do
        route $ setExtension "html"

        compile $ do
            myCompiler
            >>= saveSnapshot "content"
            >>= loadAndApplyTemplate pageTemplateIdentifier   postCtx
            >>= loadAndApplyTemplate defaultTemplateIdentifier postCtx

    match "syd_manifesto/index.html" $ do
        route idRoute

        compile $ do
            pages <- chronological =<< loadAllSnapshots sydManifestoPattern "content"
            let ctx =
                    listField "posts" sydManifestoPageCtx (return pages)
                    `mappend` sydManifestoPageCtx

            getResourceBody
                >>= applyAsTemplate ctx
                >>= loadAndApplyTemplate pageTemplateIdentifier ctx
                >>= loadAndApplyTemplate defaultTemplateIdentifier ctx


