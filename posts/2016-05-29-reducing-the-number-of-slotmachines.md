---
layout: post
title: Reducing the number of slot machines
category: Experience
tags: slot machine,time,well,spent,productivity
---

My weekly review took 6 hours this week instead of the regular single hour.

<div></div><!--more-->

I have tried getting rid of distractions for a while, but somehow they have always found their way back into my 'work'flow.
Now my productivity had been rolling downhill for a while.
Then [this article about recent technology](https://medium.com/@tristanharris/how-technology-hijacks-peoples-minds-from-a-magician-and-google-s-design-ethicist-56d62ef5edf3#.ozl5yop30) popped up on [Hacker News](https://news.ycombinator.com/).
Then I saw [this TED talk](https://www.youtube.com/embed/jT5rRh9AZf4?autoplay=1&rel=0&fs=1) and [the website on the same topic](http://timewellspent.io/) and got motivated to make some drastic improvements.

### Slot machines and distractions

The basic idea is that some of current technology is addicting because it acts exactly like a slot machine.
You pull a lever to sometimes (!) get a reward.
Apparently that is incredibly addicting to humans.
Examples include 'pull to refresh' or clicking 'receive email'.

The fact that we spend our lives at these slot machines hit me so hard that I decided to try and throw away as many of them as I can.
It has proven not to be a trivial task.

In the past I have already made an effort to get rid of distractions.
Unfortunately, getting rid of distractions the naive way will often create another slot machine and vice versa.
For example, if you turn off notifications from receiving an email, the unconscious fear of missing something important (FOMSI) can cause you to start checking it addictedly. Then it becomes a slot machine.
If you turn on notifications so you only need to check when there is something to see, then the slot machine disappears, but you will be interrupted again.

In what follows I will describe how I tried to remove the slot machine without turning them into distractions.
It is about making sure that choices I make are conscious and that they really are choices that I want to make and want to want to make.

### Low hanging fruit

#### Smart watch

A smart watch is not useful.
It is fun, a nice gadget, but not useful.
I have not missed it since I took it off.

#### Twitter

Twitter has not really brought me anything useful.
It is fun, but addictive and not a slot machine I want within my reach.

![](/assets/reducing-the-number-of-slotmachines/twitter.png)

Of course Twitter did not let me go so easily.
I will get to delete my account in thirty days as long as I do not log in anymore.
This really shows how they are trying to hijack your time.

#### Youtube

Google accounts are linked to everything made by Google, including email, so I cannot just remove it.
What I *can* do is remove all of my subscriptions.

![](/assets/reducing-the-number-of-slotmachines/unsub.png)

I will find out some way or another if there is some video that I really need to see.
No need to play the slot machine to find out.

#### Facebook

Facebook has a great 'settings' board.
Both privacy and notifications can be heavily configured.

I just set everything to 'as few notifications as possible' and turned off chat.

![](/assets/reducing-the-number-of-slotmachines/facebook.png)

### Phone

There is this 'no notifications' mode on Android phones.
Of course just turning that on would just turn my phone into another slot machine.
For this to work, I needed to first remove all slot machine apps from my phone.

Anything that has a 'slide to refresh' function has to go.
That included reddit, stackoverflow, etc...

After that, turning off all notifications just relieved more mental pressure.


### Email

Email is a necessary evil, so how can we make it less intrusive and still not a slot machine?

Removing all email notifications will turn email into a slot machine, but turning on notifications to not have to continuously refresh will make email intrusive again.
I would have to make some special arrangements.
Here is the current idea:

- I only look at email once a day, in the afternoon.
- I make sure that it is impossible to pull any slot machine levers
- I make sure that I only get interruptions that are strictly necessary.

I have a little script called `process-mail.sh` that handles everything concerning receiving email.
The first thing I did was to make sure that I do not run this script manually.
I added this to the top of the script:

``` bash
if [[ "$1" != "AUTOMATICALLY" ]]
then
  echo "You must not run this script manually!"
  exit 1
fi
```

It is not foolproof but it is a start and it ensures that I consciously choose to receive mail manually when I do.

Next, I made sure that I do not check email any sooner than the afternoon.
Instead of processing email every five minutes, like I used to, I now only process email at 16:00.

```
0 16 * * * /home/syd/scripts/process-mail.sh AUTOMATICALLY > /tmp/mail-log.log 2>&1
```

This ensures that I do not pull any levers to receive email and that I cannot be interrupted by them either.


#### No classifier after all

I thought about making a classifier for emails that could tell when an email was important enough to interrupt me right then.
I was ready to start implementing a classifier like this.
I had looked up libraries, thought about how to arrange the system and how to make the training data.
Then I realised that there would be no emails at all that I would ever classify as 'worth an interruption'.
So after all, as fun as it may have been, I will not be writing a classifier for now.


