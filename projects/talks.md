---
layout: page
title: Talks
published: 2015-07-04
last-updated: 2015-07-04
tags: notes, math
---

These are the slides for talks I've given.

<div></div><!--more-->

As you can see, I haven't given many talks.
That's something I would very much like to change so if you would like me to speak at your event, please contact me.

- [Super User Spark: A safe way to never worry about your beautifully configured system again](https://syd_kerckhove@bitbucket.org/syd_kerckhove/sus-talk.git) @ CCC-ZH 2016-04-20
- [An introduction to Haskell](https://bitbucket.org/syd_kerckhove/ccczh-intro-to-haskell) @ CCC-ZH - 2016-01-16
- [Artificiële Intelligentie, een Introductie (dutch)](https://bitbucket.org/syd_kerckhove/marnix-ai/overview) @ Marnix Ring - 2015-04-22
