---
layout: post
title: Mathematical writing
category: Mathematics
tags: technical,writing
---


Don't be shy to write more if it makes the reasoning clearer.

FIND AN EXAMPLE FROM STATISTICS


Intuition should never be strictly necessary, but it can be useful.
If you'd like to build some intuition in the text, make sure to also present a definition that requires no intuition whatsoever.

Don't write:

but write:




- Make sure numbering is monotone.

Don't write:

Definition 1.1: [...]
Definition 1.2: [...]
Lemma 1.1: [...]

but write:
Definition 1.1: [...]
Definition 1.2: [...]
Lemma 1.3: [...]

