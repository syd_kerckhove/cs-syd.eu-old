---
layout: post
title: Personal management
category: Experience
tags: personal, management, erasmus, internship
---


The single most discriminating factor in applications, whether it be for internships, for an exchange, or for any other request of exceptional treatment,
is not skill, nor clothing or charisma, but paperwork.
