---
layout: quote
author: Bill Atkinson
quote: Because I didn’t know it couldn’t be done, I was enabled to do it.
title: Because I didn’t know it couldn’t be done, I was enabled to do it. - Bill Atkinson
tags: Apple, geometry, passion, work, windows
---

Computers weren't always as easy to use as they are now.
(Take it from [someone who still uses an '80s interface](/posts/2014-07-29-why-i-dont-like-guis.html).)
There was a time that the windows on a screen were a very new and amazing feature of a personal computer.
In that time, it was an engineering marvel to have those windows overlap.

Bill Atkinson was an engineer at Apple when it was still called Apple computer.
He created the complex code to allow windows on a screen to overlap.
More interesting than the code is that Atkinson only thought it was possible because he thought he had seen it during a visit at Xerox PARC.
He succeeded in making a never-before-seen feature because he was convinced (by what was evidence to him) that it was possible.

He later admitted that, if he hadn't been convinced that it was possible to make those windows overlap, he would have quit before it was done.

This story is a great reminder of the fact that you use people's life's work every day, and that even they could only do it because they thought they could.
