---
layout: page
title: Keyboard and Layout
published: 2015-05-24
tags: keyboard,layout,kinesis,dvorak,typing
---

For my desktop system, I'm using a [Kinesis advantage](https://www.kinesis-ergo.com/shop/advantage-for-pc-mac/) keyboard.
The Kinesis Advantage offers comfort and durability.
I also use a layout based on the [Dvorak Keyboard Layout](http://www.kaufmann.no/roland/dvorak/), which I adapted for the Kinesis Advantage.

<div></div><!--more-->

To understand why I chose this layout, you might want to read ["The switch to dvorak and an ergonomic keyboard"](/posts/2014-08-31-the-switch-to-dvorak-and-an-ergonomic-keyboard.html) and ["Another new keyboard"](/posts/2015-02-08-another-new-keyboard.html).


#### Layout
I started out with a [programmer-dvorak layout](http://www.kaufmann.no/roland/dvorak/) but I changed some keys around:

- I changed the placement of the symbols on the number-row because the original placement of the brackets didn't make sense to me at all.
Now all brackets are placed symmetrically.
- I changed a lot of the other keys around the actual letters as well.
The the Kinesis Advantage is a lot more symmetrical than usual keyboards so there was no room to place the keys where they normally are.
- I changed the location of the `shift`, `control`, `alt` and `super` keys.
They're now at my thumbs, which makes more sense on a Kinesis keyboard.

```
Normal:

                      &[{+(=  *)!}]#
                      \;,.py  fgcrl/
                <shift>aoeui  dhtns<shift>
                 <ctrl>'qjkx  bmwvz<ctrl>
                       <$        -@
Shift:

                      %75319  02468`
                      |:<>PY  FGCRL?
                <shift>AOEUI  DHTNS<shift>
                 <ctrl>"QJKX  BMWVZ<ctrl>
                       >~        _^
Thumb keys:

              <super>  <alt>  <alt>  <super>
                     <empty>  <empty>
  <backspace> <tab> <delete>  <esc>  <enter> <space>
```

(If anyone knows of a way to generate a nicer picture of this, please let me know!)

To set the keyboard layout, I don't manually program the keys via the hardware 'macro' keys.
I use a very specific, hand-written [`xmodmap` file](https://github.com/NorfairKing/sus-depot/blob/master/shared/keyboards/dvorak.kinesis).
It's set in my [`.xinitrc` file](https://github.com/NorfairKing/sus-depot/blob/master/quintus/xorg/xinitrc) so that it happens automatically at X server boot:

```
$ setxkbmap ".keyboards/dvorak.kinesis"
```


#### Keycaps
I am using blank keycaps, so as to make absolutely sure that I'm typing blindly.
