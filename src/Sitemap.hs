{-# LANGUAGE OverloadedStrings #-}

module Sitemap where

import           Hakyll

import           Contexts
import           Patterns

siteMapRules :: Rules ()
siteMapRules = do
    create ["sitemap.xml"] $ do
        route idRoute
        compile $ do
            pages <- loadAll pagesPattern
            smPages <- loadAll sydManifestoPattern
            finishedContent <- loadAll finishedPattern

            let ctx =
                    listField "finishedContent" postCtx (return finishedContent) `mappend`
                    listField "sm_pages" sydManifestoPageCtx (return smPages)    `mappend`
                    listField "pages" rootCtx (return pages)                     `mappend`
                    rootCtx

            makeItem "" >>= loadAndApplyTemplate "templates/sitemap.xml" ctx


