{-# LANGUAGE OverloadedStrings #-}

module Assets where

import           Hakyll

import           Patterns

-- Assets
assetsRules :: Rules ()
assetsRules = do
    match assetsPattern $ do
        route   idRoute
        compile copyFileCompiler

cvRules :: Rules ()
cvRules = do
    match (fromList ["cv/cv.pdf", "cv/timeline.pdf"]) $ do
        route   idRoute
        compile copyFileCompiler

gpgRules :: Rules ()
gpgRules = do
    match "0x16A49ED0.txt" $ do
        route $ customRoute (\i -> "key/" ++ toFilePath i)
        compile copyFileCompiler

booksRules :: Rules ()
booksRules = do
    match (fromList myBooks) $ do
        route idRoute
        compile copyFileCompiler

    match "books/the-notes/out/*.pdf" $ do
        route idRoute
        compile copyFileCompiler

  where
    myBooks =
      [
        "books/ab-notities.pdf"
      , "books/algebra-notities.pdf"
      , "books/analyse-notities.pdf"
      , "books/kansrekenen-notities.pdf"
      , "books/meetkunde-notities.pdf"
      , "books/TMI-notities.pdf"
      , "books/all-you-can-carry.pdf"
      , "books/gegevensbanken-tutorials.pdf"
      , "books/guide.pdf"
      ]

