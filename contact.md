---
layout: page
title: Contact
header: Contact
---

We have a plethora of communication media at our disposal.
This makes it difficult to agree on protocols.
Here is mine.

<div></div><!--more-->

Before I go any further:
It is important to note that this concerns communcation addressed to me.
I will enforce certain standards on those communications.
Feel free to make up your own for yours. 

I purposely don't have an account on most social network sites.
This seems to confuse people when they want to reach me.
There is only one way to reach me reliably and that is *email*.

Before you do contact me, there are some things you need to know:

- If you expect me to address you formally, you will address me formally.
- I will read an email within 3-5 working days.
- If you put me in CC, I will not respond to the email. I will only read it.
- If I'm not the only one that the email is adressed to, I will regard it as though I was in CC.
- Only one subject per email.
- One sentence per line.
- If you send me HTML emails, they will be deleted automatically.
- Replying inline significantly boosts my willingness to read and/or reply to your email.
- If you send me any spam, your future communication addressed to me will not reach its destination.
  That includes chain mails (but not [chainmail](http://en.wikipedia.org/wiki/Mail_%28armour%29), please do send me chainmail!), 'funny' pictures, etc.

Even though I have these standards, I am not usually unpleasant.
Feel free to contact me if sincere.
I won't bite.


Here is the address:
<div class="address">
  <span class="fn email">
    syd.kerckhove<span style="display:none">muhaha</span>@gmail.com
  </span>
</div>
