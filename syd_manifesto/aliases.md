---
layout: page
title: Aliases
published: 2015-05-10
last-updated: 2015-05-30
tags: aliases,dotfiles
---

As I have mentioned, I use the [SUS Depot](/syd_manifesto/sus-depot.html) to backup and synchronise my dotfiles.
The first dotfiles I ever configured were the shell's aliases.
Some aliases are shared among my systems, and others are system specific.

I have a '.aliases' directory in my home directory.
There's a file that just contains references to all other alias files and a lot of files with each a very specific set of aliases.
While [the page about the SUS Depot](/syd_manifesto/sus-depot.html) already gave an example of why the aliases are structured in different files, I also just like multiple files better than one.

<div></div><!--more-->

```
$HOME
|- .aliases
| |- aliases  
| |- color_aliases  
| |- dotfile_aliases
| |- editor_aliases
| |- git_aliases
| |- ls_aliases
| |- navigation_aliases
| |- network_aliases
| |- package_aliases
| |- xmonad_aliases
| |- ...
|- ...
```

The ['aliases' file](https://github.com/NorfairKing/sus-depot/blob/master/shared/shell/aliases/aliases) gets sourced in the shell's '.rc' file.

``` bash
if [ -f ~/.aliases/aliases ]
then
    source ~/.aliases/aliases
fi
```

It, on its turn, sources the other alias files, and adds a few misc aliases.

``` bash
$ cat ~/.aliases/aliases
# Refresh these aliases 
alias refresh='source ~/.aliases/aliases'

# source all categorized aliases
source_safe() {
    if [ -f $1 ]
    then
        source $1
    fi
}

source_safe ~/.aliases/color_aliases
source_safe ~/.aliases/dotfile_aliases
source_safe ~/.aliases/git_aliases
source_safe ~/.aliases/ls_aliases

[...]

# Terminal aliases 
alias c='clear'
alias h='history'
alias x='exit'

# Open files easily.
alias open='xdg-open'
```

I'm only going to go over a few of the other aliases because they change over time and not all of them are interesting, the rest can be found in [my SUS depot](https://github.com/NorfairKing/sus-depot/tree/master/shared/shell/aliases).

#### Dotfiles
I edit my shell configuration files _a lot_, so I have made aliases for that too.
I actually recommend making some of these whenever you start to learn about a new dotfile.

``` bash
$ cat dotfile_aliases 
alias vbr="vim $HOME/.bashrc"
alias vbp="vim $HOME/.bash_profile"
alias vp="vim $HOME/.profile"

alias vzr="vim $HOME/.zshrc"
alias vze="vim $HOME/.zshenv"
alias vzp="vim $HOME/.zprofile"
alias vzli="vim $HOME/.zlogin"
alias vzlo="vim $HOME/.zlogout"
```

I have the same kind of aliases for some other dotfiles:

``` bash
$ cat xmonad_aliases 
alias xmd='vim $HOME/.xmonad/xmonad.hs'
alias xmb='vim $HOME/.xmobarrc'
```

#### Editors
I use my terminal for any file editing, so I shorten any editor commands as much as possible.

``` bash
$ cat editor_aliases 
alias v='vim'
alias e='emacsclient -c'
```

(Yes, both vim and emacs. You never know when I might once use emacs.)

#### Ls
Here comes my favorite feature.
I alias 'ls' with my preferred options to 'l'...

``` bash
$ cat ls_aliases 
alias l='
    clear ;
    ls  -l                              \
        -X                              \
        --color=auto                    \
        --group-directories-first       \
        --human-readable                \
        --time-style="+%d/%m/%y - %R"   \
    '
alias la='l --almost-all'
alias lr='l -R'
```

... and have it execute after every successful directory change. 

``` bash
cdls() {
  builtin cd "$*"
  if [ "$?" -eq 0 ]; then
    l
    echo $PWD > ~/.last_dir
  fi
}
alias cd='cdls'

# Restore last dir
alias coop='cd `cat ~/.last_dir`'
```

Meanwhile, cd also records which is the directory so that I can go to that directory immediately, for example in another terminal.

#### Navigation
Here are some standard aliases to navigate through directories.
I especially like the dots to navigate upwards.

``` bash
$ cat navigation_aliases 
alias hm='cd $HOME'

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
```

#### Internet
I need an easy way to check up on networking information.
If I do need to know, these aliases will do.
`ping1` checks whether I have a working internet connection while `ping2` checks whether the system can connect to my router.

``` bash
$ cat network_aliases 
alias ping1='ping -c 4 google.com'
alias ping2='ping -c 4 192.168.0.1'
```

#### System specific aliases
These aliases are a nice example of aliases that you shouldn't share across systems.

``` bash
$ cat package_aliases 
alias install='packer -S'
alias search='packer -Ss'
alias update='sudo pacman -Syu'
alias remove='sudo pacman -R'
```

#### Git
I have a lot of Git aliases, but you'll have to look at [my git setup](/syd_manifesto/git.html) to find those.
