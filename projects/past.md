---
layout: page
title: Free and open source software projects
published: 2015-07-04
last-updated: 2015-07-04
tags: projects, tools
---

This is a selection of the projects that I've started.

### Tools
- [Haphviz](https://github.com/NorfairKing/haphviz):<br> Graphviz code generation with Haskell
- [HESS](https://github.com/NorfairKing/hess):<br> Haskell E-mail Scraper Spider
- [EDEN](https://github.com/NorfairKing/eden):<br> the project Euler Development ENgine
- [Chronicle](https://github.com/NorfairKing/chronicle/tree/master):<br> A command-line journal with optional encryption 
- [Super User Spark](https://github.com/NorfairKing/super-user-spark):<br> A safe way to never worry about your beautifully configured system again
- [Super User Stone](https://github.com/NorfairKing/super-user-stone) (deprecated in favor of Super User Spark):<br> The configurations deployer that turns any computer into your computer

### Misc
- [Yi Solarized Dark Color Scheme](https://github.com/NorfairKing/yi-solarized): A dark solarized color scheme for the [Yi](https://github.com/yi-editor/yi) editor

