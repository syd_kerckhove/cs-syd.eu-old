---
layout: page
title: SUS Depot
tags: SUS, dotfiles
published: 2015-04-26
---

I have _a lot_ of dotfiles and I have put a lot of time into configuring them.
I don't want to be doing this more than once, especially because I reinstall my operating system often.

I'm using the [Super User Stone](https://github.com/NorfairKing/super-user-stone) [to synchronise and back up my dotfiles](/posts/2014-08-17-super-user-stone.html).
I back up the files via two git repositories, a [public repository](https://github.com/NorfairKing/sus-depot) for all dotfiles that may come in handy to other people and a private repository for sensitive data.

<div></div><!--more-->


#### Aliases

[My aliases](/syd_manifesto/aliases.html) are a perfect example of why I use the Super User Stone.
Some aliases, I want to share across my systems.
Other aliases, I want to make very specific for a system.

For example, I usually alias the command to install a package but not every Linux distribution has the same package manager.
On an Ubuntu system, I want an alias for `apt-get`

```
$ alias install='apt-get install'
```

while, on an Arch Linux system, I want an alias for `pacman`.
    
```
$ alias install='packer -S'
```


#### Keyboards

My keyboards are another good example.
Ony of my systems has a [kinesis advantage](/posts/2015-02-08-another-new-keyboard.html) keyboard, for which I need a different `xmodmap` file.
With the Super User Stone, I needn't worry about getting these in place.
The configuration specifies that I'd like a keyboard `xmodmap` file and the directory structure specifies which one.
