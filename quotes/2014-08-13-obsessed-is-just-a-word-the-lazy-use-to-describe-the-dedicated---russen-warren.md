---
layout: quote
author: Russel Warren
quote: Obsessed is just a word the lazy use to describe the dedicated.
title: Obsessed is just a word the lazy use to describe the dedicated. - Russel Warren
tags: passion, work
---

This might sound like one of the more pretentious of quotes, but there are a few things worth considering before making that judgement.

Passionate people are often called "obsessed" or "crazy".
They do what they do because they _love what they do_.
For them, happiness comes from _doing_ the work, not from being able to relax afterwards.

Lazy people, or any mediocre people for that matter, 
fall into the habit of calling these passionate people "obsessed" or "crazy" as an excuse.
This post is just a shout-out to the people who are passionate about their work.
Keep on doing what you do, and don't let anyone discourage you to do what you love to do.
