---
layout: page
title: Shell
published: 2016-07-02
last-updated: 2016-07-02
tags: shell, dotfiles
---

Instead of `bash`, I use `zsh` as my shell of choice.
Here is how to set it up.
<div></div><!--more-->

I use [Oh-My-Zsh](https://github.com/robbyrussell/oh-my-zsh) but mostly just for the `agnoster` theme as described in [this dotfile](https://github.com/NorfairKing/sus-depot/blob/master/shared/shell/zsh/zshrc#L6).
Installing Oh-My-Zsh is simply described in [its readme](https://github.com/robbyrussell/oh-my-zsh/blob/master/README.md).

Note that it is important that I [deploy my dotfiles](http://cs-syd.eu/syd_manifesto/sus-depot.html) after installing Oh-My-Zsh.
This ensures that the custom parts of my `zsh` configuration are installed automatically.

Then all that remains is to make `zsh` the default shell:

```
chsh -s /usr/bin/zsh
```
