{-# LANGUAGE OverloadedStrings #-}

module GeneralCfg where


--[ General config ]------------------------------------------------------------
blogRoot :: String
blogRoot = "http://cs-syd.eu"

blogTitle :: String
blogTitle = "CS Syd"

blogDescription :: String
blogDescription = "Computer Science Syd"

language :: String
language = "en"

author :: String
author = "Tom Sydney Kerckhove"

github :: String
github = "NorfairKing"

twitter :: String
twitter = "KerckhoveSyd"

