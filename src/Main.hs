module Main where

import           System.Environment (getArgs)

import           Deploy             (checkoutToday, postReady)
import           Post               (setUpPost, setUpQuote)
import           Site               (site)

main :: IO ()
main = do
    args <- getArgs
    let command = head args
    let function = lookup command executeList
    case function of
        Nothing -> site
        Just execute -> execute

executeList :: [(String, IO())]
executeList =
    [
        ("post"  , setUpPost)
    ,   ("quote" , setUpQuote)
    ,   ("post-ready" , postReady)
    ,   ("checkout-today" , checkoutToday)
    ]

