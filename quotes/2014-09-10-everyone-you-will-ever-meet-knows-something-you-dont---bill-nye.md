---
layout: quote
author: Bill Nye
quote: Everyone you will ever meet knows something you don't.
title: Everyone you will ever meet knows something you don't. - Bill Nye
tags: learning, judgement
---

I have always thought this was a beautiful call for tolerance among 'smart people'.
By 'smart people', in this case, I don't mean actual smart people.
I mean people who fancy themselves so accomplished that they need not learn any more, especially from other people whom they deem intellectually inferior. 

This is a reminder for all the 'smart' people.
The communicational barrier that you experience when talking to people you think are 'less intelligent' is felt in almost the same way at the other end.
You can, however, take the responsibility to improve this communication by opening your mind for what you can learn from any single person.
Once you get into this habit, you might enjoy these conversations orders of magnitude more than before while exercising your patience.
