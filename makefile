all: site assets

site: FORCE
	stack install

.PHONY: assets
FORCE:

assets:
	$(MAKE) -C cv
	$(MAKE) -C books

clean:
	rm -f $(EXECUTABLE)
