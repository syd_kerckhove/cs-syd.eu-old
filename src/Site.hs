{-# LANGUAGE OverloadedStrings #-}

module Site where

import qualified Data.Map                        as M
import           Data.Time
import           Hakyll
import           System.Environment              (getArgs)
import           System.FilePath.Posix           (takeFileName)
import           Text.Blaze.Html                 (toHtml, toValue, (!))
import           Text.Blaze.Html.Renderer.String (renderHtml)
import qualified Text.Blaze.Html5                as H
import qualified Text.Blaze.Html5.Attributes     as A

import           Assets
import           Compilers
import           Constants
import           Contexts
import           Feed
import           Patterns
import           Routes
import           Sitemap
import           Style
import           SydManifesto
import           TechnicalCfg
import           Utils

--[ Hakyll configuration ]------------------------------------------------------
config :: Configuration
config = defaultConfiguration   {
                                    deployCommand = deployCmd
                                ,   destinationDirectory = outputDir
                                }

--[ Rules ] -------------------------------------------------------------------

site :: IO ()
site = hakyllWith config $ do
    styleRules
    assetsRules
    cvRules
    booksRules
    gpgRules
    templatesRules

    frontpageRules
    postRules
    quotesRules

    archiveRules
    pagesRules
    tagsPageRules
    categoriesPageRules
    projectsRules
    sydManifestoRules

    feedRules
    siteMapRules

    draftsRules

templatesRules :: Rules ()
templatesRules = match templatesPattern $ compile templateCompiler

-- Front page
frontpageRules :: Rules ()
frontpageRules = do
    match "index.html" $ do
        routeId

        compile $ do
            latestPosts <- fmap (take 10) . recentFirst =<< loadAllSnapshots contentPattern "content"
            let indexCtx =
                    listField "postExerpts" postCtx (return $ tail latestPosts) `mappend`
                    listField "latest" postCtx (return $ take 1 latestPosts) `mappend`
                    rootCtx

            getResourceBody
                >>= applyAsTemplate indexCtx
                >>= loadAndApplyTemplate defaultTemplateIdentifier indexCtx


-- Quotes
quotesRules :: Rules ()
quotesRules  = do
    match "quotes.html" $ do
        routeId

        compile $ do
            quotes <- recentFirst =<< loadAll quotesPattern
            let quotesPageCtx =
                    listField "quotes" postCtx (return quotes) `mappend`
                    constField "title" "Quotes"                `mappend`
                    postCtx

            getResourceBody
                >>= applyAsTemplate quotesPageCtx
                >>= loadAndApplyTemplate pageTemplateIdentifier    quotesPageCtx
                >>= loadAndApplyTemplate defaultTemplateIdentifier quotesPageCtx

    match quotesPattern $ do
        routeOut

        compile $ do
            myCompiler
            >>= saveSnapshot "content"
            >>= loadAndApplyTemplate quoteTemplateIdentifier   postCtx
            >>= loadAndApplyTemplate defaultTemplateIdentifier postCtx

-- Pages
pagesRules :: Rules ()
pagesRules = do
    match (fromList ["about.md", "contact.md", "404.md", "todo.md"]) $ do
        routeOut
        compile $ pandocCompiler
            >>= loadAndApplyTemplate pageTemplateIdentifier  rootCtx
            >>= loadAndApplyTemplate defaultTemplateIdentifier rootCtx

    match missingPagePattern $ do
        routeId
        compile $ do
            getResourceBody
                >>= applyAsTemplate rootCtx
                >>= loadAndApplyTemplate defaultTemplateIdentifier rootCtx

-- Archive page
archiveRules :: Rules ()
archiveRules = do
    match "archive.html" $ do
        routeId

        compile $ do
            posts <- recentFirst =<< loadAll contentPattern
            let archiveCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    constField "title" "Archives"            `mappend`
                    rootCtx

            getResourceBody
                >>= applyAsTemplate archiveCtx
                >>= loadAndApplyTemplate pageTemplateIdentifier archiveCtx
                >>= loadAndApplyTemplate defaultTemplateIdentifier archiveCtx

-- Tags page
tagsPageRules :: Rules ()
tagsPageRules = do
    match "tags.html" $ do
        routeId

        compile $ do
            tags <- buildTags taggedPattern (fromCapture "tags/*.html")
            let tagsCtx =
                    constField "title" "Tags"            `mappend`
                    tagCloudCtx (sortTagsBy caseInsensitiveTags tags) `mappend`
                    rootCtx

            getResourceBody
                >>= applyAsTemplate tagsCtx
                >>= loadAndApplyTemplate pageTemplateIdentifier tagsCtx
                >>= loadAndApplyTemplate defaultTemplateIdentifier tagsCtx

-- Categories page
categoriesPageRules :: Rules ()
categoriesPageRules = do
    match "categories.html" $ do
        routeId

        compile $ do
            categories <- buildTagsWith getMetadataCategory categorisedPattern (fromCapture "categories/*.html")
            let catCtx =
                    constField "title" "Categories"      `mappend`
                    catCloudCtx categories               `mappend`
                    rootCtx
            getResourceBody
                >>= applyAsTemplate catCtx
                >>= loadAndApplyTemplate pageTemplateIdentifier catCtx
                >>= loadAndApplyTemplate defaultTemplateIdentifier catCtx

catCloudCtx :: Tags -> Context String
catCloudCtx tags = field "tagcloud" $ \_ -> renderCatList tags

renderCatList :: Tags -> Compiler (String)
renderCatList = renderTags makeLink (renderHtml . H.ul . mconcat . map (H.li . H.preEscapedToHtml) )
  where
    makeLink ::  (String -> String -> Int -> Int -> Int -> String)
    makeLink tag url count _ _ = renderHtml $
        H.a ! A.href (toValue url) $ toHtml (tag ++ " (" ++ show count ++ ")")



postRules :: Rules ()
postRules = do
    match "posts/*" $ do
        routeOut

        compile $ do
            myCompiler
            >>= saveSnapshot "content"
            >>= loadAndApplyTemplate postTemplateIdentifier    postCtx
            >>= loadAndApplyTemplate defaultTemplateIdentifier postCtx


    -- Build a page tags/tag.html for each 'tag'
    tags <- buildTags taggedPattern (fromCapture "tags/*.html")
    -- Builds the tags page
    tagsRules tags $ \tag pattern -> do
        let title = "Posts tagged \"" ++ tag ++ "\""

        route idRoute

        compile $ do
            posts <- recentFirst =<< loadAll pattern
            let ctx = constField "title" title
                      `mappend` listField "posts" postCtx (return posts)
                      `mappend` rootCtx

            makeItem ""
                >>= loadAndApplyTemplate "templates/categories.html" ctx
                >>= loadAndApplyTemplate pageTemplateIdentifier ctx
                >>= loadAndApplyTemplate defaultTemplateIdentifier ctx

    categories <- buildTagsWith getMetadataCategory categorisedPattern (fromCapture "categories/*.html")
    tagsRules categories $ \cat pattern -> do
        let title = "Posts in category \"" ++ cat ++ "\""
        routeId

        compile $ do
            posts <- recentFirst =<< loadAll pattern
            let ctx = constField "title" title
                    `mappend` listField "posts" postCtx (return posts)
                    `mappend` rootCtx

            makeItem ""
                >>= loadAndApplyTemplate "templates/tags.html" ctx
                >>= loadAndApplyTemplate pageTemplateIdentifier ctx
                >>= loadAndApplyTemplate defaultTemplateIdentifier ctx

    years <- buildTagsWith (\i -> getPostDate i >>= return . return . getYear) contentPattern (fromCapture "*.html")
    tagsRules years $ \year pattern -> do
        let title = "Posts in the year " ++ year

        months <- buildTagsWith (\i -> getPostDate i >>= return . return . getMonth) contentPattern (fromCapture . fromGlob $ year ++ "/*.html")
        tagsRules months $ \month pattern -> do
            let title = "Posts in " ++ month ++ " of " ++ year
            route idRoute
            compile $ do
                posts <- chronological =<< loadAll pattern
                let ctx = constField "title" title
                        `mappend` listField "posts" postCtx (return posts)
                        `mappend` rootCtx

                makeItem ""
                    >>= saveSnapshot "content"
                    >>= loadAndApplyTemplate "templates/month.html" ctx
                    >>= loadAndApplyTemplate pageTemplateIdentifier ctx
                    >>= loadAndApplyTemplate defaultTemplateIdentifier ctx

        route idRoute

        compile $ do
            posts <- chronological =<< loadAll pattern
            let ctx = constField "title" title
                    `mappend` listField "posts" postCtx (return posts)
                    `mappend` rootCtx

            makeItem ""
                >>= loadAndApplyTemplate "templates/year.html" ctx
                >>= loadAndApplyTemplate pageTemplateIdentifier    ctx
                >>= loadAndApplyTemplate defaultTemplateIdentifier ctx

draftsRules :: Rules ()
draftsRules =
    match draftsPattern $ do
        routeOut

        compile $ do
            myCompiler
            >>= saveSnapshot "content"
            >>= loadAndApplyTemplate postTemplateIdentifier    postCtx
            >>= loadAndApplyTemplate defaultTemplateIdentifier postCtx

projectsRules :: Rules ()
projectsRules = do
    match projectSuggestionsPattern $ do
        routeOut

        compile $ do
            myCompiler
            >>= saveSnapshot "content"
            >>= loadAndApplyTemplate "templates/project-suggestion.html"    projectsPageCtx
            >>= loadAndApplyTemplate defaultTemplateIdentifier              projectsPageCtx

    match projectsPattern $ do
        routeOut

        compile $ do
            myCompiler
            >>= saveSnapshot "content"
            >>= loadAndApplyTemplate pageTemplateIdentifier    postCtx
            >>= loadAndApplyTemplate defaultTemplateIdentifier postCtx

    match "projects/index.html" $ do
        routeOut

        compile $ do
            pages <- chronological =<< loadAllSnapshots projectSuggestionsPattern "content"
            let ctx =
                    listField "posts" projectsPageCtx (return pages)
                    `mappend` projectsPageCtx

            getResourceBody
                >>= applyAsTemplate ctx
                >>= loadAndApplyTemplate pageTemplateIdentifier ctx
                >>= loadAndApplyTemplate defaultTemplateIdentifier ctx





--------------------------------------------------------------------------------

tagCloudCtx :: Tags -> Context String
tagCloudCtx tags = field "tagcloud" $ \_ -> rendered
    where rendered = renderTagCloud 60.0 300.0 tags

getMetadataCategory :: MonadMetadata m => Identifier -> m [String]
getMetadataCategory identifier = do
    metadata <- getMetadata identifier
    return $ case lookupString "category" metadata of
            Nothing  -> ["No category"]
            Just cat -> [cat]

getPostLastModifiedDate :: MonadMetadata m => Identifier -> m UTCTime
getPostLastModifiedDate identifier = do
    metadata <- getMetadata identifier
    case lookupString "last-updated" metadata of
        Just updated -> return $ parseTimeOrError True defaultTimeLocale "%Y-%m-%d" updated
        Nothing -> getPostDate identifier

getPostDate :: MonadMetadata m => Identifier -> m UTCTime
getPostDate identifier = do
    metadata <- getMetadata identifier
    let dateString = case lookupString "published" metadata of
                    Just published -> published
                    Nothing -> take (length ("2015-03-22" :: String)) $ takeFileName $ toFilePath identifier
    let timeFromString = parseTimeOrError True defaultTimeLocale "%Y-%m-%d" dateString :: UTCTime
    return timeFromString
