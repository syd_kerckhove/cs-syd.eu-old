---
layout: post
title: Thalia, the muse of my drama
category: Experience
tags: love, drama
---

And now for something completely different.
I'd like to dedicate a post to my lovely girlfriend: Thalia.

I won't go into how incredibly attractive she is, on any level. 
Although that subject could fill entire books on its own, I feel it would be inappropriate to discuss it here.
Neither will I go into all the little quirks that I love about her, like the way she smiles when she gets nervous.
In this post, I will try to explain what makes her such an amazing person, as a way of showing her my appreciation, as well as encouraging everyone to dare to step outside of the norm.

<div></div><!--more-->

### Peer pressure
The first aspect of Thalia that I find admirable is that she doesn't simply conform to what people expect of her.
She will not apologise for being herself, nor will she pretend to be someone else, just because it's easier to do so.
I hope - and I imagine she does too - that she can serve as an example for people to be the best they can be.

### Adventure
The reason I'm writing this here, rather than just telling her how amazing she is in person, is that she is currently at the other end of the world.
Thalia has always had a knack for adventure.
She studied in Valencia, Spain for five months in her third year at university.
Somehow, she managed to learn to read, write and speak Spanish fluently by the time she got back.
Now, she is in Cuenca, Ecuador at an internship for the university, working on the improvement of citizen participation and neighberhood development.

### Morals
Morals and values had always baffled me.
I could never understand why anyone would want any.
They haven't confused her though.
She is the only one I know with such high moral standards without ever failing to meet them herself.
Furthermore, she won't impose any of her moral standards on anyone who minds his own business.

Only to give an example of one of those moral standards: Thalia has an absolute zero tolerance for prejudice.
She hasn't accepted, nor will she ever accept, any racism, sexism, or any form of discrimination for that matter.

### Good
As you might imagine, I am absolutely crazy about this girl.
She is the most worthwhile, thoroughly good person I have ever met.
She has inspired me to be a better man, a better person, a better leader.
I wish her, and anyone who wishes to follow her example, the best of luck and a life of great fulfilment.

