---
layout: page
title: OSPIP
description: Open-Source Project Idea Platform
state: Unstarted
published: 2016-01-02
last-updated: 2016-01-02
tags: OSPIP, Open-Source, project, platform
---

The idea for this project came to me when I published my second project suggestion on this website.
Most programmers have a lot more ideas than they can ever work on.

> “The best way to have a good idea is to have lots of ideas.”
> ― Linus Pauling

Moreover, it is hard to find out whether a given idea is worth working on.

What if we had a way to share these ideas and figure out whether they were worth working on?

<div></div><!--more-->

### The idea

The idea is to build a web platform where programmers can share their project ideas and find other people to work on the projects together.

There would only have to be two buttons on every idea.
One that says 'I would like to work on this project' that would take you to an application page and one that says 'This project would have a positive impact on my life'.
Maybe a 'WTF were you thinking' button could also come in handy.

Programmers could share their ideas for anyone to work on, they could find other developers to work with and they could find out whether their idea is worth working on.

Ideas could be entirely new systems, extensions or improvement to existing systems but they would have to be open-source projects.

### Requirements

OSPIP has to be free and open-source.

This is all about free and open-source software.
The platform would have to communicate very clearly that, in sharing your ideas, you would give others the permission to act upon them as well.
A legal-person go over the details of the 'ownership' of ideas before this platform could be built.

The focus has to be on making good ideas a reality, rather than making money off them.


### Suggestions

I am a big Haskell fan, so I would suggest yesod or something similar, but there are far more experienced web-developers out there that I would gladly take advice from.
