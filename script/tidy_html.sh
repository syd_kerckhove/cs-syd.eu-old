#!/bin/bash
command="tidy -config script/tidy.conf"

# rest="cat"
# ignore_warning () {
#   rest="$rest | sed '/$1/d'"
# }
# 
# ignore_warning "Warning: <script> proprietary attribute \"async\""
# ignore_warning "Warning: <ins> proprietary attribute \"data-ad-client\""
# ignore_warning "Warning: <ins> proprietary attribute \"data-ad-slot\""

tmp=/tmp/html.txt
find _site -name "*.html" > $tmp

founderror=""
while read i
do
  errs=$($command "$i" 2>&1 \
    | sed '/Warning: <script> proprietary attribute "async"/d' \
    | sed '/Warning: <ins> proprietary attribute "data-ad-client"/d' \
    | sed '/Warning: <ins> proprietary attribute "data-ad-slot"/d' \
    | sed '/Warning: <a> proprietary attribute "property"/d' \
    | sed '/Warning: <a> proprietary attribute "xmlns:cc"/d' \
    | sed '/Warning: <span> proprietary attribute "xmlns:dct"/d' \
    | sed '/Warning: <span> proprietary attribute "href"/d' \
    | sed '/Warning: <span> proprietary attribute "rel"/d' \
    | sed '/Warning: <span> proprietary attribute "property"/d' \
    | sed '/Warning: <img> lacks "alt" attribute/d' \
    | sed '/Warning: trimming empty <div>/d')
  if [[ "$errs" ]]; then
    echo $i
    echo $errs
    founderror=True
  fi
done < $tmp

if [[ "$founderror" ]]
then
  exit 1
fi
