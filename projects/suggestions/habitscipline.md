---
layout: page
title: Habitscipline
description: Command line habit tracker
state: Early Design
published: 2015-09-06
last-updated: 2015-10-23
tags: habit, tracker, CLI
---

Creating and maintaining habits seems to be the key to doing what you want to want to do.
There are a lot of habit trackers out there.
Most of them are web-based and have graphical user interface.

<div></div><!--more-->

You can find the project on Github: [Habitscipline](https://github.com/NorfairKing/habitscipline)

### The idea
The goal of this project is to create a system that allows the users to create and maintain good habits and get rid of bad habits.

It requires the habit to track your habits for such a system to work.
The system also needs to be designed so that it will become a habit for the user to track its habits.

Because there already exist so many web-based, GUI-based habit trackers, it is important that this system tailors to power-users and integrates well with their system.


### Requirements

This program could be a mobile application but it must at least be a desktop application.
Either way it must function offline perfectly.
The desktop application should work on Linux systems.

The application’s source code must be free and open source.
The application must be free of all advertisements and downloading it must be entirely free of charge.

The program should have a command-line interface.
All the functions should be accessible via a command-line interface, both in human-readable and computer-readable format.
This will allow the (power-)user to integrate the habit tracker with the rest of their system as they wish.

Habits are 'global' in the scope of a user.
For the sake of total immersions, the application would benefit from additional realisations with a centralised data store.
That's a very complicated way of saying: a mobile application would be good too.
The application should allow (power-)users to take the responsibility of managing that centralised data store.

The system should be extremely configurable.
The configuration should be textual and human readable when stored.


### Suggestions

This application, at least the local application, is a good starter-project for anyone trying to learn a new language.

Haskell seems like a very good candidate for a language to write this application in.
The opt-parse applicative library is perfect for the user interface.
The Aeson JSON library allows for easy serialisation to- and from JSON.

For communication with a possible server, Protobuffer protocols seem perfect.


