---
title: How to set up a taskserver on your amazon EC2 instance
category: Tutorial
tags: taskwarrior,amazon,VPS,EC2
---
# packages

```
sudo yum install gnutls uuid make gcc cmake gnutls-devel libuuid-devel gcc-c++ gnutls-utils

~ $ git clone https://git.tasktools.org/scm/tm/taskd.git taskd
```

# installation
```
~ $ cd taskd

~/taskd $ cmake -DCMAKE_BUILD_TYPE=release -DCMAKE_CXX_COMPILER=g++ .
~/taskd $ make

~/taskd $ cd test
~/taskd/test $ make
~/taskd/test $ ./run_all

~/taskd/test $ cd ..
~/taskd $ sudo make install
```

# initialisation
pick `TASKDDATA=~/task-data` (mind the double D)

```
~/taskd $ cd ..
~/ $ export TASKDDATA=~/task-data
~/ $ mkdir -p $TASKDDATA
~/ $ taskd init
You must specify the 'server' variable, for example:
  taskd config server localhost:53589

Created /home/ec2-user/task-data/config
```


# keys & certs
self signed, not secure but ok for me.


```
$ vim taskd/pki/generate.server
dns_name = "cs-syd.eu"
ip_address = "54.88.129.203"
```

```
$ ./taskd/pki/generate.ca
$ ./taskd/pki/generate.server
$ ./taskd/pki/generate.crl
$ ./taskd/pki/generate.client
```

```
$ cat copy-certs
export TASKDDATA=~/task-data
cp client.cert.pem $TASKDDATA
cp client.key.pem  $TASKDDATA
cp server.cert.pem $TASKDDATA
cp server.key.pem  $TASKDDATA
cp server.crl.pem  $TASKDDATA
cp ca.cert.pem     $TASKDDATA

taskd config --force client.cert $TASKDDATA/client.cert.pem
taskd config --force client.key $TASKDDATA/client.key.pem
taskd config --force server.cert $TASKDDATA/server.cert.pem
taskd config --force server.key $TASKDDATA/server.key.pem
taskd config --force server.crl $TASKDDATA/server.crl.pem
taskd config --force ca.cert $TASKDDATA/ca.cert.pem
```

# server config

```
$ cd $TASKDDATA/..
$ taskd config --force log $PWD/taskd.log
$ taskd config --force pid.file $PWD/taskd.pid
$ taskd config --force server localhost:53589
$ taskd config --force client.allow '^task [2-9]'
```

check settings with 
`taskd config`

# Client config on server

#org
```
taskd add org cs-syd
```
# user
```
$ taskd add user cs-syd 'Tom Sydney Kerckhove'
New user key: e31803a9-6e1c-429b-be50-xxxxxxxxxxxx
Created user 'Tom Sydney Kerckhove' for organization 'cs-syd'
```
# copy the user key

# generate client cert
```
./taskd/pki/generate.client tom_sydney_kerckhove
```

# client config

```
scp blog:~/tom_sydney_kerckhove.cert.pem ~/.task
scp blog:~/tom_sydney_kerckhove.key.pem ~/.task
scp blog:~/ca.cert.pem ~/.task
```

```
$ task config taskd.certificate -- ~/.task/tom_sydney_kerckhove.cert.pem
$ task config taskd.key         -- ~/.task/tom_sydney_kerckhove.key.pem
$ task config taskd.ca          -- ~/.task/ca.cert.pem

$ task config taskd.server      -- cs-syd.eu:53589

$ task config taskd.credentials -- cs-syd/Tom Sydney Kerckhove/e31803a9-6e1c-429b-be50-03aa17d663ec
```

# turn on
```
$ taskd server --daemon
```

# syncing
```
$ task sync init
```

# normal sync:
```
$ task sync
```

# put in crontab


```
$ cat start_task.sh
export TASKDDATA=~/task-data
taskd server --daemon
```

```
$ cat stop_task.sh
export TASKDDATA=~/task-data
```

