{-# LANGUAGE OverloadedStrings #-}

module Post where

import           System.Directory   (getCurrentDirectory)
import           System.FilePath    (takeDirectory)
import           System.IO          (hFlush, stdout)
import           Text.Printf        (printf)

import           Data.Char          (isSpace, toLower)
import           Data.List          (intersperse)
import           Data.List.Split    (splitOn)
import           Data.Maybe         (fromJust)

import           Data.Time.Calendar (toGregorian)
import           Data.Time.Clock    (getCurrentTime, utctDay)

import           Constants

currentDate :: IO (Integer,Int,Int) -- :: (year,month,day)
currentDate = getCurrentTime >>= return . toGregorian . utctDay

data Content = Content [(String, String)]

instance Monoid Content where
    mempty = Content []
    mappend (Content m1) (Content m2) = Content $ m1 ++ m2

constructContent :: Content -> String
constructContent (Content cmap) = "---\n" ++ concatMap (\(k,v) -> k ++ ": " ++ v ++ "\n") cmap ++ "---\n"

getPostContent :: IO Content
getPostContent = do
    title <- getTitle
    cat <- getCategory
    tagsList <- getTags
    let tags = concat $ intersperse "," tagsList
    return $ Content [("layout","post"),("title", title),("category",cat),("tags",tags)]

getQuoteContent :: IO Content
getQuoteContent = do
    quote <- getQuote
    author <- getAuthor
    let title = quote ++ " - " ++ author
    tagsList <- getTags
    let tags = concat $ intersperse "," tagsList
    return $ Content [("layout","quote"),("quote",quote),("author",author),("title",title),("tags",tags)]

getDestDir :: IO FilePath
getDestDir = do
    pwd <- getCurrentDirectory
    let dir = pwd ++ draftsDirectory
    return dir

constructContentFileName :: Content -> (Integer, Int, Int) -> FilePath
constructContentFileName (Content cmap) date = publishedFileName date $ (++ ".md") $ map toLower $ concat $ intersperse "-" $ words $ sanitise title
    where
        title = fromJust $ lookup "title" cmap
        sanitise :: String -> String
        sanitise = (filter . flip notElem) (",':;.?" :: String)

setUpContent :: Content -> IO ()
setUpContent metaData = do
    destDir <- getDestDir
    now <- currentDate
    let post = constructContent metaData
    let postFileName = constructContentFileName metaData now
    let file = destDir ++ "/" ++ postFileName
    writeFile file post

setUpPost :: IO ()
setUpPost = getPostContent >>= setUpContent

setUpQuote :: IO ()
setUpQuote = getQuoteContent >>= setUpContent

publishedFileName :: (Integer, Int, Int) -> FilePath -> FilePath
publishedFileName (y,m,d) file = (++ "-" ++ file) $ concat $ intersperse "-"
    [
        printf "%04d" y
    ,   printf "%02d" m
    ,   printf "%02d" d
    ]

promptString :: String -> IO String
promptString prompt = do
    putStr $ prompt ++ "> "
    hFlush stdout
    getLine

getTitle :: IO String
getTitle = promptString "title"

getCategory :: IO String
getCategory = promptString "category"

getAuthor :: IO String
getAuthor = promptString "author"

getQuote :: IO String
getQuote = promptString "quote"

getTags :: IO [String]
getTags = do
    line <- promptString "tags"
    return $ map trim $ splitOn "," line

trim :: String -> String
trim = f . f
   where f = reverse . dropWhile isSpace
