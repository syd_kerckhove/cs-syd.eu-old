module Utils where

import           Data.Time


(</) :: String -> FilePath
(</) s = '/':s

getYear :: UTCTime -> String
getYear time = formatTime defaultTimeLocale "%Y" time

getMonth :: UTCTime -> String
getMonth time = formatTime defaultTimeLocale "%B" time

