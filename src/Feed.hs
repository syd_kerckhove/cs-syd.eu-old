{-# LANGUAGE OverloadedStrings #-}

module Feed where

import           Contexts
import           GeneralCfg
import           Hakyll
import           Patterns

feedConfiguration :: String -> FeedConfiguration
feedConfiguration title = FeedConfiguration
    {
        feedTitle = blogTitle ++ " - " ++ title
    ,   feedDescription = blogDescription
    ,   feedAuthorName = author
    ,   feedAuthorEmail = "syd.kerckhove@gmail.com"
    ,   feedRoot = blogRoot
    }

feedCtx :: Context String
feedCtx = mconcat
    [
        bodyField "description"
    ,   postCtx
    ]

feedRules :: Rules ()
feedRules = do
    create ["rss.xml"] $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAllSnapshots contentPattern "content"
            renderRss (feedConfiguration "All Posts") feedCtx posts

