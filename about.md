---
layout: page
title: About
header: About
---

Hi! My name is Tom Sydney Kerckhove.

I'm a computer science student exchange student at ETH, Zurich in Switzerland.
I am particularly interested building extremely safe software, ideally using fully automated proofs.
