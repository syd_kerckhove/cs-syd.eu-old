---
layout: page
title: Introducing the Syd Manifesto
tags: background, history, Linux, dotfiles, OS
published: 2015-04-19
---

I started using linux at the end of 2012 because I wanted to try dual booting it with windows and somehow I couldn't get Windows to work anymore.
Eversince then I have only used linux.
It was confusing in the beginning (and not so much less confusing now), but after a week, I never wanted to go back to my old operating system.

After a week, I started using config files.
I had looked up how to change something about an application and found out how to do that by adding some lines to a file.
Not a week later, something went wrong.
The only thing I knew I could do to solve it was to reinstall my operating system.
(You may now facepalm.)
The week after that, I added the previously mentioned lines to the config file again.

<div></div><!--more-->

I started using non-default tools and found out that they too use config files.
Whenever I wanted to change something, I would look up how to do it and add a few lines to the config files.
I ended up with more than ten files that started with a dot and ended in 'rc'.

Then, one day, my laptop gave up on me.
It wouldn't even boot anymore and just started beeping uncontrolably.
I brought my laptop to the store where I bought it for repairs.
I hadn't made backups. 
When I got it back and booted it up, there was a windows loading screen.
I was a little angry at first, but I installed linux again.

I was annoyed to see that all the config files that I put together were gone again.
I started thinking about how I could prevent having to rewrite all my config files everytime I reinstalled my operating system.
It was around that time that I got a second computer with linux.
At first, I copied all dotfiles to my USB drive and pasted them on the other system.

Of course, whenever I changed a config file on one system, I would have to copy it over again.
Then I started putting my config files in a dropbox folder, and I made a symlink to them.
This way, all my config files would be synchronised across my systems.
Now, the only thing left to do about my config files whenever I reinstalled my operating system was make the links.

This soon became tedious too. Then I made the [Super User Stone](https://github.com/NorfairKing/super-user-stone).
This program was made just to get any of my systems back into working state after reinstalling the operating system.

As it turns out, there are some parts of my setup that I can't just write and synchronize config files for.
For these parts, I needed to look up the same tutorials every time, extract the information that I want and implement that.
I find myself in need of my own tutorials so that I can refer to them when I am reinstalling my operating system.
This is why I am writing the Syd manifesto.
I require a reference system for my setup.
Documentation, if you will.
Additionally, I find that the tutorials I write are better when I intend to publish them.
In this way, the Syd manifesto will ensure some quality in my tutorials, as well as force me to keep my config files clean and secure.

The Syd manifesto will continue to grow as I further document my system and it will be updated as I change my system.

EDIT: 2015-08-08
The [Super User Stone](https://github.com/NorfairKing/super-user-stone) has been deprecated in favor of the [Super User Spark](https://github.com/NorfairKing/super-user-spark).
