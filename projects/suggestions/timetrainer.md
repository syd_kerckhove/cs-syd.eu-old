---
layout: page
title: TimeTrainer
description: Creating incentives for personal behavior improvements one second at a time
state: Unstarted
published: 2015-08-16
last-updated: 2015-08-16
tags: TimeTrainer, time, trainer, habit
---

The idea for this project was the first one I ever tried implementing when I had just started learning how to program.
Back then it was a non-version controlled, goop of spaghetti code that happened to function sometimes.
I have since also lost the source code but I would like to rebuild this system, as it was of great personal use to me back then.

<div></div><!--more-->

### The idea
The goal of the project is to build a system that allows users to create incentives for themselves to do what they want to want to do in exchange of doing things that they like.
That is a _very_ abstract way to explain the idea so let me explain how this instantiated itself in my first implementation.

The program was an offline desktop application that displayed a clock.
The clock did not show a point in time but rather an amount of time.
At the time I wanted to start exercising and I liked gaming.
The goal was to exchange exercise for gaming.
I entered the exercises I'd done into the program and I would get more time on the clock.
When I wanted to game, I would start the clock and I had to stop when it would run out of time.
This gave me a strong incentive to exercise (a lot) more.

### Requirements

This program could be a mobile application or a desktop application but it must be function offline perfectly.
If it is a desktop application it should work on at least Windows, Linux and Mac systems.

The application's source code will be free and open source.
The application must be free of _all_ advertisements and downloading it will be entirely free of charge.

The interface will be graphical but [my objections to GUI's](/posts/2014-07-29-why-i-dont-like-guis.html) should be taken into consideration.
This means that there should be configurable keyboard shortcuts and the appearance should be heavily configurable.

The central concept in the application is that time is to be earned and spent.
No other currency should be used, especially real currencies.

In my example I used exercise and gaming.
Which concepts are used to earn time and to spend time should be entirely configurable.
Not only the concepts but also the concrete ways should be configurable.
In the example of exercise, it should be configurable that doing push-ups is a way to earn time and how much a push-up is worth should be configurable as well.

There is a soft requirement for earnables to change in value based on how frequent they are achieved.
The way these values change should be configurable as well.
Consider the example of exercise again.
Doing a lot of push-ups should decrease their value and increase the value of sit-ups but shouldn't decrease the value of running.

A lot of parts of this application seem to have to be configurable.
The configuration should be textual and human readable when stored.


### Suggestions

Because of the environment requirement, it seems reasonable to consider using a JVM language if it becomes a desktop application.
