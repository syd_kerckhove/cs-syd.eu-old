module Constants where

import           System.FilePath.Posix ((<.>), (</>))

import           Hakyll

import           Utils


--[ Scale ]--

initial_scale :: Double
initial_scale = 1.0


--[ Extensions ]--

outputExtension :: String
outputExtension = "html"

templateExtension :: String
templateExtension = outputExtension

postExtension :: String
postExtension = "md"


--[ Names ]--

missingPageName :: String
missingPageName = "404"

templatesDirectoryName :: String
templatesDirectoryName = "templates"

defaultTemplateName :: String
defaultTemplateName = "default"

postTemplateName :: String
postTemplateName = "post"

quoteTemplateName :: String
quoteTemplateName = "quote"

postsDirectoryName :: String
postsDirectoryName = "posts"

quotesDirectoryName :: String
quotesDirectoryName = "quotes"

assetsDirectoryName :: String
assetsDirectoryName = "assets"

draftsDirectoryName :: String
draftsDirectoryName = "drafts"

styleDirectoryName :: String
styleDirectoryName = "css"

pageTemplateName :: String
pageTemplateName = "page"

missingPage :: FilePath
missingPage = missingPageName <.> templateExtension



--[ Templates ]--

template :: String -> FilePath
template name = templatesDirectoryName </> name <.> templateExtension

templateIdentifier :: String -> Identifier
templateIdentifier name = fromFilePath $ templatesDirectoryName </> name <.> templateExtension

defaultTemplate :: FilePath
defaultTemplate = template defaultTemplateName

postTemplate    :: FilePath
postTemplate    = template postTemplateName

quoteTemplate :: FilePath
quoteTemplate = template quoteTemplateName

pageTemplate    :: FilePath
pageTemplate    = template pageTemplateName

defaultTemplateIdentifier :: Identifier
defaultTemplateIdentifier = templateIdentifier defaultTemplateName

postTemplateIdentifier :: Identifier
postTemplateIdentifier = templateIdentifier postTemplateName

quoteTemplateIdentifier :: Identifier
quoteTemplateIdentifier = templateIdentifier quoteTemplateName

pageTemplateIdentifier :: Identifier
pageTemplateIdentifier = templateIdentifier pageTemplateName


--[ Directories ]--

templatesDirectory :: FilePath
templatesDirectory = (</) templatesDirectoryName

postsDirectory :: FilePath
postsDirectory = (</) postsDirectoryName

quotesDirectory :: FilePath
quotesDirectory = (</) quotesDirectoryName

assetsDirectory :: FilePath
assetsDirectory = (</) assetsDirectoryName

draftsDirectory :: FilePath
draftsDirectory = (</) draftsDirectoryName


styleDirectory :: FilePath
styleDirectory = (</) styleDirectoryName

outputDir :: FilePath
outputDir = "_site"

--[ Logo ]--
logoFileName :: String
logoFileName = "logo"

logoFile :: FilePath
logoFile = assetsDirectory </> logoFileName <.> "png"

--[ Style ]--

styleSheetName :: String
styleSheetName = "style"

styleSheetFile :: FilePath
styleSheetFile = styleSheetName <.> "css"

styleSheet :: FilePath
styleSheet = styleDirectory </> styleSheetFile

styleSheetIdentifier :: Identifier
styleSheetIdentifier = fromFilePath $ styleDirectoryName </> styleSheetFile

styleSheetSourceFile :: FilePath
styleSheetSourceFile = styleSheetName <.> "less"

styleSheetSource :: FilePath
styleSheetSource = styleDirectory </> styleSheetSourceFile

styleSheetSourceIdentifier :: Identifier
styleSheetSourceIdentifier = fromFilePath $ styleDirectoryName </> styleSheetSourceFile
