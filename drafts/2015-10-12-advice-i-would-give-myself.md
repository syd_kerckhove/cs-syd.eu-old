---
layout: post
title: Advice I would give myself
category: Opinion
tags: advice,oppisition,extra
---

You will face extra opposition because you want to do more and they want you to be average.
No-one wants to deal with you if you're making an extra effort

HOWEVER, don't forget to be grateful for the people that help you because they believe in what you're trying to do.
