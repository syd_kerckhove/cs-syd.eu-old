---
layout: page
title: Git
published: 2015-05-31
tags: git,aliases,dotfiles
---

Even Git needs some configuration.
Here are the ones that I use.
Currently I haven't made a lot of custom configurations, but I'm convinced that more will follow.

<div></div><!--more-->


### Gitconfig

At the moment, the only important lines in [my `.gitconfig` file](https://github.com/NorfairKing/sus-depot/blob/master/shared/git/gitconfig) are the `core` lines:

```
[core]
    editor = vim
    excludesfile = ~/.gitignore_global
```

The most important part is the `excludesfile` configuration.


### Global ignore
I think it's very important to not do stupid things _by accident_.
The `gitignore_global` file contains instructions on what kinds of files should never be committed.

Temporary editor files should never be committed:
```
# Vim
*~
.*.sw[op]

# Emacs
\#*\#
.\#*
```

Obviously there are a lot more types of files that should never end up in a repository, for the full file, see [my `.gitignore_global` file](https://github.com/NorfairKing/sus-depot/blob/master/shared/git/gitignore).


### Aliases

I use git more than any other program so time-saving aliases are important to me.

Note that some of these aliases only work safely when you've excluded the right files by default (see the global ignore section above)
The `g` alias is a great example of this.
This alias is a very unsafe shortcut if you don't know what you're doing, but can be a huge timesaver otherwise.

``` bash
alias g='git add --all . && git commit -a'
```

Some aliases are just very short versions of frequently used commands.

``` bash
alias gs='git status'
alias gd='git diff'

alias ga='git add'
alias gaf='git add -f'

alias gam='git commit --amend'
alias gc='git commit'
```

Pushing and pulling:

``` bash
alias gp='git push'
alias gpl='git pull'

function gr() {
    git tag -a $1 -m $2
}
alias gr=gr
alias gpr='git push origin --tags'
```

The let's hop around and do things aliases:

``` bash
alias gm='git merge --no-ff'
alias gmd='gm development'
alias gmm='git merge master'

alias gb='git branch'
alias gco='git checkout'
alias gcm='git checkout master'
alias gcd='git checkout development'
alias gnb='git checkout -b'

alias gpb='git push --set-upstream origin'
```

Of course, you modify the `.gitignore` file at time, don't you?
Save about 6 keystrokes there as well:

``` bash
alias gi="vim .gitignore"
```

The 'gl' and 'gls' aliases are for some nice features that most people don't regularly use because they're too long to type.

``` bash
alias gl='git log --graph --decorate --abbrev-commit --all --pretty=oneline'
alias gls='git log --graph --decorate --abbrev-commit --all --pretty=oneline --stat'
```

I know that it's possible to configure git aliases in the `.gitconfig` file as well, but then the alias must be preceded by `git` and I want to shorten that as well.
That's why I configure git aliases like all other aliases.
