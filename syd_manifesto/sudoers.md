---
layout: page
title: The sudoers file
published: 2015-08-08
last-updated: 2015-08-08
tags: xmonad
---

I can put most configuration files in my sus-depot.
The sudoers file needs root permissions to access, however.
Like most of my system I configure it anyway and that's why I have to keep a record of it here.

<div></div><!--more-->


`sudo visudo` gets you into the sudoers file if you have access to it.
At this point it comes in handy that you learned how to use `vi`!

Here's my `/etc/sudoers` file:

```
# Root can do everything
root  ALL=(ALL) ALL

# Anyone in the sudo group can do everything
%sudo ALL=(ALL) ALL

# Some command should need a password on a single user system
%sudo ALL=(ALL) NOPASSWD: /sbin/shutdown
%sudo ALL=(ALL) NOPASSWD: /sbin/reboot
%sudo ALL=(ALL) NOPASSWD: /sbin/rtcwake
```

As you can see, it's very simple.
`root` and anyone in the `sudo` group can do anything as usual.

Because I don't like to use GUI's, I turn off and reboot my computer from the command line as well.
Somehow I always felt entitled to being able to shut down my computer without typing a password and now that's a reality.
