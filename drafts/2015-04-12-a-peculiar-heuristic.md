---
layout: post
title: A peculiar heuristic
category: Opinion
tags: experience,heuristic,efficiency,random
---

Suppose you are at a road junction.
Either one of the two roads will lead you to your destination eventually, but one of them is significantly longer than the other and you don't know which.
What do you do?

I've heard many different answers.
Among the most common are the following:

- Figure out which one of the roads is the shortest and take that one.
- Always take the left road. If you do this enough times, you will travel the shortest road about half of the time.
- Go down one road, figure out if it was the right one along the way and travel back when you conclude that the other road was shorter (factoring the distance you've already traveled of course).

Obviously this is not a strictly mathematical question.
It is too vague to be so.

I would approach this situation entirely differently.
This is predicated on the goals that I persue
I would pick a random road.
