---
layout: quote
quote: You don't want to finish life knowing you've never been an additional agenda item.
author: Zuber Anwar
title: You don't want to finish life knowing you've never been an additional agenda item. - Zuber Anwar
tags: agenda,life
---

This isn't as much a famous quote as it is a life motto from a good friend.
Don't be afraid to be an additional agenda item.
Don't be afraid to be deviant.

<div></div><!--more-->

From a young age our whole lives seem to be laid out for us.
Because this is a consequence of the fact that we were fortunate enough to be born rich, we are expected and taught to be grateful for this 'guidance'.
Any deviation from this 'ideal' path is frowned upon and considered ungrateful.
We are quickly branded as weird as soon as it becomes clear that we aspire to deviate from the norm.
It is considered rude and problematic.
Next thing we know, we are an additional agenda item in the weekly meeting of the ones that lay out that path.

Years later those who grew up in this kind of environment will either resent it and move away or help maintain this imprisonment of averages.
The entire culture now embodies this pursuit of mediocrity and the dangers of such a toxic environment could well make up a shelf of books.

Because you're reading this, I will assume that you are one of the people who has not been rigorously average their entire life.
At some point you must have felt guilty.
Being warned about the fact that you were a separate agenda item will have felt at least a fraction as bad as it was meant to make you feel.
Of course when you think about it, the greater fear would be to never be a separate agenda item.

Ultimately you will regret having been average more than you will regret pushing further.
Knowing you conformed is never a defining moment.

