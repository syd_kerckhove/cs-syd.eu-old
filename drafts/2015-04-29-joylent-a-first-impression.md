---
layout: post
title: Joylent, a first impression
category: joylent,impression,first
tags: Experience
---

In the last few months I started to develop an eating problem.
I started eating less and less up to the point where I ate less than one meal a day.
I was rarely hungry and didn't take the time to make and eat a decent meal.
I won't go into why this happened.
Instead of making excuses for this poor choice of habits, I will descibe how I fixed my problem.

<div></div><!--more-->

### The problem
My problem could be reduced to the transaction costs of preparing and eating meals:

- Buying ingredients
- Making sure the ingredients haven't gone bad by the time you use them
- Preparing the meal
- Eating the food
- Cleaning up

Enter Joylent
